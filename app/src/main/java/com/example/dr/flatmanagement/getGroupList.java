package com.example.dr.flatmanagement;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class getGroupList extends AsyncTask<Void, Void, Void> {

    String data = "";
    String dataPared = "";
    String single = "";
    HttpURLConnection openCon;

    @Override
    protected Void doInBackground(Void... voids) {

        try {

            URL url = new URL("http://212.182.24.41:8000/groupsOfItems/");

            openCon = (HttpURLConnection) url.openConnection();

            InputStream inputStream = openCon.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";
            while (line != null) {
                line = bufferedReader.readLine();
                data = data + line;
            }

            JSONArray JA = new JSONArray(data);
            for (int i = 0; i < JA.length(); ++i) {
                JSONObject jo = (JSONObject) JA.get(i);
                single = " " + jo.get("nameGroup") + "\n";
                dataPared = dataPared + single;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }        finally {
            openCon.disconnect();
        }
        return  null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        MainActivity.textView.setText(dataPared);
    }
}
